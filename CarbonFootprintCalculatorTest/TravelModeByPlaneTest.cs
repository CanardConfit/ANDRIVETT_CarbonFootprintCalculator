using CarbonFootprintCalculator;
using NUnit.Framework;

namespace CarbonFootprintCalculatorTest
{
    public class TravelModeByPlaneTest
    {
        [Test]
        public void Assert_AvailableTravelClasses_Contains_All()
        {
            TravelModeByPlane obj = new TravelModeByPlane(285);
            Assert.Contains("Classe économique", obj.AvailableTravelClasses);
            Assert.Contains("Classe affaire"   , obj.AvailableTravelClasses);
            Assert.Contains("Première classe"  , obj.AvailableTravelClasses);
        }
        
        [Test]
        public void Assert_ComputeCarbonFootprint_Work()
        {
            TravelModeByPlane obj = new TravelModeByPlane(285);
            foreach (string travelClass in obj.AvailableTravelClasses)
            {
                Assert.AreEqual((obj.RatiosForTravelClasses[travelClass] * 10 * obj.GramCo2PerKm), obj.ComputeCarbonFootprint(10, travelClass));
            }
        }
    }
}