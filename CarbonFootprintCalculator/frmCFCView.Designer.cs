﻿namespace CarbonFootprintCalculator
{
    partial class frmCFCView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxParcours = new System.Windows.Forms.GroupBox();
            this.lblStartDest = new System.Windows.Forms.Label();
            this.lblDe = new System.Windows.Forms.Label();
            this.cbxDestA = new System.Windows.Forms.ComboBox();
            this.lblA = new System.Windows.Forms.Label();
            this.btnCalcul = new System.Windows.Forms.Button();
            this.gbxEmpreinte = new System.Windows.Forms.GroupBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.gbxMoyenTransport = new System.Windows.Forms.GroupBox();
            this.rbtnTrain = new System.Windows.Forms.RadioButton();
            this.rbtnAvion = new System.Windows.Forms.RadioButton();
            this.cbxClasse = new System.Windows.Forms.ComboBox();
            this.lblClasse = new System.Windows.Forms.Label();
            this.gbxParcours.SuspendLayout();
            this.gbxEmpreinte.SuspendLayout();
            this.gbxMoyenTransport.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxParcours
            // 
            this.gbxParcours.Controls.Add(this.lblStartDest);
            this.gbxParcours.Controls.Add(this.lblDe);
            this.gbxParcours.Controls.Add(this.cbxDestA);
            this.gbxParcours.Controls.Add(this.lblA);
            this.gbxParcours.Location = new System.Drawing.Point(12, 12);
            this.gbxParcours.Name = "gbxParcours";
            this.gbxParcours.Size = new System.Drawing.Size(262, 198);
            this.gbxParcours.TabIndex = 0;
            this.gbxParcours.TabStop = false;
            this.gbxParcours.Text = "Parcours";
            // 
            // lblStartDest
            // 
            this.lblStartDest.Location = new System.Drawing.Point(80, 47);
            this.lblStartDest.Name = "lblStartDest";
            this.lblStartDest.Size = new System.Drawing.Size(121, 23);
            this.lblStartDest.TabIndex = 5;
            this.lblStartDest.Text = "Genève";
            // 
            // lblDe
            // 
            this.lblDe.Location = new System.Drawing.Point(18, 47);
            this.lblDe.Name = "lblDe";
            this.lblDe.Size = new System.Drawing.Size(44, 23);
            this.lblDe.TabIndex = 4;
            this.lblDe.Text = "De :";
            // 
            // cbxDestA
            // 
            this.cbxDestA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDestA.FormattingEnabled = true;
            this.cbxDestA.Location = new System.Drawing.Point(80, 129);
            this.cbxDestA.Name = "cbxDestA";
            this.cbxDestA.Size = new System.Drawing.Size(121, 24);
            this.cbxDestA.TabIndex = 2;
            // 
            // lblA
            // 
            this.lblA.Location = new System.Drawing.Point(18, 132);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(44, 23);
            this.lblA.TabIndex = 1;
            this.lblA.Text = "A :";
            // 
            // btnCalcul
            // 
            this.btnCalcul.Location = new System.Drawing.Point(12, 216);
            this.btnCalcul.Name = "btnCalcul";
            this.btnCalcul.Size = new System.Drawing.Size(533, 42);
            this.btnCalcul.TabIndex = 3;
            this.btnCalcul.Text = "Calculer l\'empreinte carbone";
            this.btnCalcul.UseVisualStyleBackColor = true;
            this.btnCalcul.Click += new System.EventHandler(this.btnCalcul_Click);
            // 
            // gbxEmpreinte
            // 
            this.gbxEmpreinte.Controls.Add(this.lblResult);
            this.gbxEmpreinte.Location = new System.Drawing.Point(12, 264);
            this.gbxEmpreinte.Name = "gbxEmpreinte";
            this.gbxEmpreinte.Size = new System.Drawing.Size(533, 100);
            this.gbxEmpreinte.TabIndex = 1;
            this.gbxEmpreinte.TabStop = false;
            this.gbxEmpreinte.Text = "Empreinte carbone du parcours (aller simple et par personne)";
            // 
            // lblResult
            // 
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblResult.Location = new System.Drawing.Point(18, 18);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(509, 79);
            this.lblResult.TabIndex = 4;
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxMoyenTransport
            // 
            this.gbxMoyenTransport.Controls.Add(this.rbtnTrain);
            this.gbxMoyenTransport.Controls.Add(this.rbtnAvion);
            this.gbxMoyenTransport.Controls.Add(this.cbxClasse);
            this.gbxMoyenTransport.Controls.Add(this.lblClasse);
            this.gbxMoyenTransport.Location = new System.Drawing.Point(283, 12);
            this.gbxMoyenTransport.Name = "gbxMoyenTransport";
            this.gbxMoyenTransport.Size = new System.Drawing.Size(262, 198);
            this.gbxMoyenTransport.TabIndex = 1;
            this.gbxMoyenTransport.TabStop = false;
            this.gbxMoyenTransport.Text = "Moyen de transport";
            // 
            // rbtnTrain
            // 
            this.rbtnTrain.Location = new System.Drawing.Point(6, 128);
            this.rbtnTrain.Name = "rbtnTrain";
            this.rbtnTrain.Size = new System.Drawing.Size(88, 24);
            this.rbtnTrain.TabIndex = 8;
            this.rbtnTrain.TabStop = true;
            this.rbtnTrain.Text = "Train";
            this.rbtnTrain.UseVisualStyleBackColor = true;
            // 
            // rbtnAvion
            // 
            this.rbtnAvion.Location = new System.Drawing.Point(6, 44);
            this.rbtnAvion.Name = "rbtnAvion";
            this.rbtnAvion.Size = new System.Drawing.Size(88, 24);
            this.rbtnAvion.TabIndex = 7;
            this.rbtnAvion.TabStop = true;
            this.rbtnAvion.Text = "Avion";
            this.rbtnAvion.UseVisualStyleBackColor = true;
            this.rbtnAvion.CheckedChanged += new System.EventHandler(this.rbtnAvion_CheckedChanged);
            // 
            // cbxClasse
            // 
            this.cbxClasse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxClasse.FormattingEnabled = true;
            this.cbxClasse.Location = new System.Drawing.Point(100, 44);
            this.cbxClasse.Name = "cbxClasse";
            this.cbxClasse.Size = new System.Drawing.Size(121, 24);
            this.cbxClasse.TabIndex = 6;
            // 
            // lblClasse
            // 
            this.lblClasse.Location = new System.Drawing.Point(100, 18);
            this.lblClasse.Name = "lblClasse";
            this.lblClasse.Size = new System.Drawing.Size(121, 23);
            this.lblClasse.TabIndex = 6;
            this.lblClasse.Text = "Classe";
            this.lblClasse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmCFCView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 376);
            this.Controls.Add(this.gbxEmpreinte);
            this.Controls.Add(this.gbxMoyenTransport);
            this.Controls.Add(this.btnCalcul);
            this.Controls.Add(this.gbxParcours);
            this.Name = "frmCFCView";
            this.Text = "Calculateur d\'empreinte carbone";
            this.gbxParcours.ResumeLayout(false);
            this.gbxEmpreinte.ResumeLayout(false);
            this.gbxMoyenTransport.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.ComboBox cbxClasse;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblClasse;
        private System.Windows.Forms.Label lblDe;
        private System.Windows.Forms.Label lblStartDest;
        private System.Windows.Forms.RadioButton rbtnAvion;
        private System.Windows.Forms.RadioButton rbtnTrain;

        private System.Windows.Forms.Button btnCalcul;
        private System.Windows.Forms.ComboBox cbxDestA;
        private System.Windows.Forms.GroupBox gbxParcours;
        private System.Windows.Forms.GroupBox gbxEmpreinte;
        private System.Windows.Forms.GroupBox gbxMoyenTransport;
        private System.Windows.Forms.Label lblA;

        #endregion
    }
}