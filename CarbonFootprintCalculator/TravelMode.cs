﻿namespace CarbonFootprintCalculator
{
    public abstract class TravelMode
    {
        protected double _gramCO2PerKm;

        public double GramCo2PerKm
        {
            get => _gramCO2PerKm;
            set => _gramCO2PerKm = value;
        }

        protected TravelMode(double gramCO2PerKm)
        {
            _gramCO2PerKm = gramCO2PerKm;
        }

        public double ComputeCarbonFootprint(int distance)
        {
            return _gramCO2PerKm * distance;
        }
    }
}