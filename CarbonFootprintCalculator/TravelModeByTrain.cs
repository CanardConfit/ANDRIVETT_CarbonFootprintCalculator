﻿namespace CarbonFootprintCalculator
{
    public class TravelModeByTrain : TravelMode
    {
        public TravelModeByTrain(double gramCO2PerKm) : base(gramCO2PerKm)
        {
        }
    }
}