﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace CarbonFootprintCalculator
{
    public partial class frmCFCView : Form
    {
        private CFCModel _model;

        private CFCModel Model
        {
            get => _model;
            set => _model = value;
        }

        public frmCFCView()
        {
            InitializeComponent();
            _model = new CFCModel();

            cbxClasse.DataSource = _model.TravelModeByPlane.AvailableTravelClasses;
            cbxDestA.DataSource = _model.Destinations.Select(dest=>dest.City).ToList();
            
            rbtnAvion.Checked = true;
        }

        private void btnCalcul_Click(object sender, EventArgs e)
        {
            string city = (string) cbxDestA.SelectedItem;
            string classs = (string) cbxClasse.SelectedItem;
            
            var result = (rbtnAvion.Checked ? _model.GetCarbonFootprintByPlane(city, classs) : _model.GetCarbonFootprintByTrain(city)) / 1000.0;

            lblResult.Text = $"{result:0.0} kg de CO2";
        }

        private void rbtnAvion_CheckedChanged(object sender, EventArgs e)
        {
            cbxClasse.Enabled = rbtnAvion.Checked;
        }
    }
}