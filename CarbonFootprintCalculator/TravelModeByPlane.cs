﻿using System;
using System.Collections.Generic;

namespace CarbonFootprintCalculator
{
    public class TravelModeByPlane : TravelMode
    {
        private Dictionary<string, double> _ratiosForTravelClasses;
        private List<string> _availableTravelClasses;

        public Dictionary<string, double> RatiosForTravelClasses
        {
            get => _ratiosForTravelClasses;
            set => _ratiosForTravelClasses = value;
        }

        public List<string> AvailableTravelClasses
        {
            get => _availableTravelClasses;
            set => _availableTravelClasses = value;
        }

        public TravelModeByPlane(double gramCO2PerKm) : base(gramCO2PerKm)
        {
            _ratiosForTravelClasses = new Dictionary<string, double>();
            _availableTravelClasses = new List<string>();
            
            _ratiosForTravelClasses.Add("Classe économique", 1);
            _ratiosForTravelClasses.Add("Classe affaire"   , 4);
            _ratiosForTravelClasses.Add("Première classe"  , 8);
            
            _availableTravelClasses.AddRange(_ratiosForTravelClasses.Keys);
        }

        public double ComputeCarbonFootprint(int distance, string travelClass)
        {
            if (_availableTravelClasses.Contains(travelClass))
                return _gramCO2PerKm * distance * _ratiosForTravelClasses[travelClass];
            throw new ArgumentException("Travel Class not an Available Travel Classes");
        }
    }
}