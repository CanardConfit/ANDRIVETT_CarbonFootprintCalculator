﻿using System.Collections.Generic;
using System.Linq;

namespace CarbonFootprintCalculator
{
    public class CFCModel
    {
        private List<Destination> _destinations;
        private TravelModeByPlane _travelModeByPlane;
        private TravelModeByTrain _travelModeByTrain;

        public List<Destination> Destinations
        {
            get => _destinations;
            set => _destinations = value;
        }

        public TravelModeByPlane TravelModeByPlane
        {
            get => _travelModeByPlane;
            set => _travelModeByPlane = value;
        }

        public TravelModeByTrain TravelModeByTrain
        {
            get => _travelModeByTrain;
            set => _travelModeByTrain = value;
        }

        public CFCModel()
        {
            _destinations = new List<Destination>();
            _destinations.AddRange(new []
            {
                new Destination("Barcelone", 623),
                new Destination("Lisbonne", 1500),
                new Destination("Londres", 746),
                new Destination("Paris", 410),
                new Destination("Zurich", 277)
            });

            _travelModeByPlane = new TravelModeByPlane(285);
            _travelModeByTrain = new TravelModeByTrain(14);
        }

        public int GetDistanceFromGeneva(string city) => _destinations.First(dest => dest.City == city).DistanceFromGeneva;

        public double GetCarbonFootprintByPlane(string city, string travelClass) => _travelModeByPlane.ComputeCarbonFootprint(GetDistanceFromGeneva(city), travelClass);

        public double GetCarbonFootprintByTrain(string city) => _travelModeByTrain.ComputeCarbonFootprint(GetDistanceFromGeneva(city));
    }
}