﻿namespace CarbonFootprintCalculator
{
    public class Destination
    {
        private string _city;
        private int _distanceFromGeneva;

        public string City
        {
            get => _city;
            set => _city = value;
        }

        public int DistanceFromGeneva
        {
            get => _distanceFromGeneva;
            set => _distanceFromGeneva = value;
        }

        public Destination() : this("Zurich", 277)
        {
            
        }

        public Destination(string city, int distanceFromGeneva)
        {
            _city = city;
            _distanceFromGeneva = distanceFromGeneva;
        }
    }
}